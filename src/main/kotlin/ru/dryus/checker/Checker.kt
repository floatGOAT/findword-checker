package ru.dryus.checker

import ru.dryus.data.WordEntry

interface Checker {
    fun check(text: String, words: Collection<String>): Collection<WordEntry>
}