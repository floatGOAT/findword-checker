package ru.dryus.checker

import ru.dryus.data.WordEntry

class DefaultChecker : Checker {
    override fun check(text: String, words: Collection<String>): Collection<WordEntry> {
        return words.map {
            val entryIndex = text.indexOf(it)
            WordEntry(it, entryIndex)
        }
    }
}