package ru.dryus.data

@kotlinx.serialization.Serializable
data class WordEntry(val word: String, val positionInText: Int) {
    val id: Int = word.hashCode()
}
